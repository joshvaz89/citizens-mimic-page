# citizens-forgot-account

Forgot Accounts/Password mimic page for Citizens.

## What's in this repository? ##

This project contains the files and tools to run the mimic page, following the Citizens Design System and ADA Compliance.

The compiler used to compile the Sass files to CSS is Gulp, while using the http-server package to run the project locally on port 8080.

## Installation Requirements ##
* [NodeJS](https://nodejs.org/en/download/)

## Running the project ##

In the repo run the following commands to install and run the page locally on port 8080:

```
npm install
npm start
```
