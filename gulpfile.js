const { src, dest, series } = require('gulp');
const sass = require('gulp-sass');

// compile `.scss` files located in a `/src/scss` folder and copy to `/dist`
const _compileScss = done => {
    src([__dirname + '/src/scss/*.scss'])
        .pipe(
            sass({
                includePaths: ['node_modules'],
                outputStyle: 'expanded',
            }).on('error', sass.logError)
        )
        .pipe(dest(__dirname + '/dist/css'));
    done();
};

// copy image assets to `/dist`
const _copyImages = done => {
    src(['./src/images/**/*']).pipe(dest(__dirname + '/dist/images'));
    done();
};

// copy scripts assets to `/dist`
const _copyScripts = done => {
    src(['./src/scripts/**/*']).pipe(dest(__dirname + '/dist/scripts'));
    done();
};

// copy HTML assets to `/dist`
const _copyHtml = done => {
    src(['./src/html/**/*.html']).pipe(dest(__dirname + '/dist'));
    done();
};

// define default build sequence
exports.default = series(_compileScss, _copyImages, _copyScripts, _copyHtml);