var form = document.getElementById('form');
var continueButton = document.getElementById('continueButton');
var cancelButton = document.getElementById('cancelButton');

// Validate form on submit
var validateForm = function() {
    var value = document.forms['form']['radio'].value;

    // Return validation status
    return !!value;
}

// Form on change, check values
var checkValues = function () {
    var value = document.forms['form']['radio'].value;

    // Change disable state of continue button based on value
    continueButton.disabled = !(!!value);
}

// Reset form and disable continue button when cancel button is clicked
var resetForm = function () {
    form.reset();
    continueButton.disabled = true;
}

form.onsubmit = validateForm;
form.onchange = checkValues;
cancelButton.onclick = resetForm;